SRC = videx_videoterm_v2.4.s

all: stock.bin mine.bin
	@echo Done.

%.o:
	ca65 --target apple2 -D$(basename $@) -o $@ $(SRC)

%.bin:	%.o
	ld65 --config apple2-asm.cfg -o $@ $<

clean:
	rm -f *.bin *.o
