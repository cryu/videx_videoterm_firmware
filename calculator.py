#! /usr/bin/env python3

# Adapted from:
#       https://worldradiohistory.com/hd2/IDX-UK/Technology/Technology-Modern/Archive-Elektor-IDX/IDX/80s/Elektor-1984-10-OCR-Page-0026.pdf    
#       https://worldradiohistory.com/hd2/IDX-UK/Technology/Technology-Modern/Archive-Elektor-IDX/IDX/80s/Elektor-1984-10-OCR-Page-0027.pdf    
#       https://worldradiohistory.com/hd2/IDX-UK/Technology/Technology-Modern/Archive-Elektor-IDX/IDX/80s/Elektor-1984-10-OCR-Page-0028.pdf    

import argparse
import math

class crtc_period_registers:
    # all registers initialized to zero
    r = [0] * 16

    def elektor(self, osc_freq = 16, sync_width = 8, rows = 24, columns = 80, 
            scan_lines_per_character = 9, osc_divisor = 8, ntsc = 0, total_char = 128):
        if ntsc == 1:
            print("generating for NTSC")
            line_frequency = 15750 # NTSC line frequency Hz
            frame_frequency = 60
        else:
            print("generating for PAL")
            line_frequency = 15625 # PAL line frequency Hz
            frame_frequency = 50
        time_sweep_one_line = (1 / line_frequency) * 10 ** 6
        time_sweep_complete_frame = (1 / frame_frequency) * 10 ** 6

        self.r[3] = sync_width          # but $58 in viewmaster, $29 in stock
        clock_period = time_sweep_one_line / (self.r[0] + 1)
        calc_osc_freq = osc_divisor / clock_period
        print("optimal frequency {} MHz (divisor {})".format(calc_osc_freq, osc_divisor))
        print("using frequency {} MHz (divisor {})".format(osc_freq, osc_divisor))
        print("  Time to sweep one line: {} μs".format(time_sweep_one_line))
        print("  Time to sweep complete frame: {} μs".format(time_sweep_complete_frame))
        print()

        clock_period = 1 / (osc_freq / osc_divisor)
        print("  Clock period: {} μs".format(clock_period))

        print()
        print("Horizontal timing:")
        line_sync_pulse_width = self.r[3] * clock_period
        print("  Line sync pulse width: {} μs".format(int(line_sync_pulse_width)))
        line_sync_pulse_period = total_char * clock_period
        print("  Line sync pulse period: {} μs".format(int(line_sync_pulse_period)))

        # r0
        self.r[0] = total_char - 1

        # r1
        self.r[1] = columns
        horiz_total_display_time = int(total_char * clock_period)
        horiz_display_time = int(columns * clock_period)
        horiz_blank_interval = horiz_total_display_time - horiz_display_time
        print("  Horizontal sync width: {} μs".format(sync_width))
        print("  Horizontal total time: {} μs".format(int(horiz_total_display_time)))
        print("  Horizontal display time (dt, usable window): {} μs".format(int(horiz_display_time)))
        print("  Horizontal blank interval time: {} μs ({}%)".format(int(horiz_blank_interval), int(horiz_blank_interval / horiz_total_display_time * 100)))

        # r2
        horiz_position = ((line_sync_pulse_period - horiz_display_time - 1.5 * line_sync_pulse_width) / 2) + horiz_display_time
        print("  Position of line sync pulse (hp): {} μs".format(int(horiz_position)))
        self.r[2] = horiz_position/clock_period

        print()

        # r4
        char_line_period = scan_lines_per_character * line_sync_pulse_period
        print("  Character line period: {} μs".format(int(char_line_period)))
        vt = (rows + 1) * char_line_period
        if vt > time_sweep_complete_frame:
            print("impossible!", sync_width, rows, char_line_period, vt)
        lines_between_frame_sync_pulses = int(time_sweep_complete_frame / char_line_period)
        print("  Lines between frame sync pulses: {} lines".format(int(lines_between_frame_sync_pulses)))
        self.r[4] = lines_between_frame_sync_pulses - 1

        raster_sync_period = lines_between_frame_sync_pulses * char_line_period + self.r[5] * line_sync_pulse_period
        print("  Raster sync period: {} μs".format(int(raster_sync_period)))

        # r5
        self.r[5] = int( (time_sweep_complete_frame - lines_between_frame_sync_pulses * char_line_period) / line_sync_pulse_period)

        # r6
        self.r[6] = rows
        vert_display_time = rows * char_line_period
        print("  Vertical display time: {} μs".format(int(vert_display_time)))

        # r7
        self.r[7] = int( ( ( (char_line_period * lines_between_frame_sync_pulses + line_sync_pulse_period * self.r[5] ) - (1500 + rows * char_line_period)) / 2 + rows * char_line_period) / char_line_period)
        vert_position = self.r[7] * char_line_period
        print("  Vertical position: {} μs".format(int(vert_position)))

        # r9
        self.r[9] = scan_lines_per_character - 1

        # r10
        self.r[10] = 0xe0
        # r11
        self.r[11] = 8

        print()

        return self.r

def main():
    parser = argparse.ArgumentParser(description='Videx Videoterm (and clones) CRTC timing generator')
    parser.add_argument('-f', '--oscillator-frequency', type=float, dest='osc_freq', help='board oscillator frequency (default 17.430MHz)', default=17.430)
    parser.add_argument('-s', '--sync-width', type=int, dest='sync_width', help='horizontal sync pulse width (default 8μs)', default=8)
    parser.add_argument('-r', '--rows', type=int, dest='rows', help='character rows (default 24)', default=24)
    parser.add_argument('-c', '--columns', type=int, dest='columns', help='character columns (default 80)', default=80)
    parser.add_argument('-l', '--scan-lines', type=int, dest='scan_lines_per_character', help='scan lines per character (default 9)', default=9)
    parser.add_argument('-d', '--oscillator-divisor', type=int, dest='osc_divisor', help='oscillator divisor (default 9)', default=9)
    parser.add_argument('-n', '--ntsc', type=int, dest='ntsc', help='use NTSC timing (default 1)', default=1)
    parser.add_argument('-t', '--total characters', type=int, dest='total_chars', help='total horizontal characters (default 128)', default=128)
    args = parser.parse_args()

    crtc_period = crtc_period_registers()
    registers = crtc_period.elektor(
        osc_freq = args.osc_freq,
        osc_divisor=args.osc_divisor,
        sync_width=args.sync_width,
        rows=args.rows,
        columns=args.columns,
        scan_lines_per_character=args.scan_lines_per_character,
        ntsc=args.ntsc,
        total_char=args.total_chars
    )
#    registers = crtc_period.elektor(osc_freq = 17.430, osc_divisor=9, sync_width = 88, ntsc=1) works, horiz offset about +50%
#    registers = crtc_period.elektor()
    for i in range(0,16):
        print("    .byte ${:02x}\t; R{}\t{}".format(int(crtc_period.r[i]), i, int(crtc_period.r[i]) ))

if __name__=='__main__':
    main()
